package com.xw.baselib;

import android.app.Application;

//import androidx.lifecycle.LifecycleObserver;
//import androidx.lifecycle.ProcessLifecycleOwner;

import com.google.android.gms.ads.MobileAds;

public class AdApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MobileAds.initialize(this, initializationStatus -> { });

//        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }
}
