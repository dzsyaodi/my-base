package com.xw.baselib;

public interface OnAdLoadListener {

    void onStartLoading();

    void onLoadFinish();

    void onError();

    void onAdClose();
}
