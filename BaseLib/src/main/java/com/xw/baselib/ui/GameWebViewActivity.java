package com.xw.baselib.ui;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebViewClient;
import com.xw.baselib.databinding.ActivityGameWebBinding;

public class GameWebViewActivity extends AppCompatActivity {

    private ActivityGameWebBinding binding;
    private AgentWeb mAgentWeb;
    private String loadUrl = "";
    private Handler handler = new Handler();
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGameWebBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        loadUrl = getIntent().getStringExtra("load_url");
        if (TextUtils.isEmpty(loadUrl)) {
            Toast.makeText(this, "loadUrl is null", Toast.LENGTH_SHORT).show();
            finish();
        }

//        loadAD();

        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(binding.frameLayoutWeb,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT))
                .closeIndicator()
                .setWebViewClient(webViewClient)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)
                        .interceptUnkownUrl()
                        .createAgentWeb()
                        .ready()
                        .go(loadUrl);

        WebSettings webSettings = mAgentWeb.getAgentWebSettings().getWebSettings();
        webSettings.setLoadWithOverviewMode(true);
        //下面这些
        webSettings.setSupportZoom(true);//设置可以支持缩放
        webSettings.setBuiltInZoomControls(true);//设置出现缩放工具
        webSettings.setUseWideViewPort(true);//扩大比例的缩放
        webSettings.setDisplayZoomControls(false);//隐藏缩放控件
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setTextZoom(100);
        webSettings.setJavaScriptEnabled(true);

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (webView != null) {
                webView.loadUrl("javascript:" + setJS(0));
                webView.loadUrl("javascript:" + setJS(1));
                webView.loadUrl("javascript:" + setJS(2));

                handler.postDelayed(runnable,10);
            }
        }
    };

    private WebViewClient webViewClient = new WebViewClient(){

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            if (url.contains("https://html5.gamemonetize.co")) {
                return super.shouldOverrideUrlLoading(view, request);
            } else {
                return true;
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            webView = view;
            handler.post(runnable);
        }
    };

    private String setJS(int type) {

        String js = "var newscript = document.createElement(\"script\");";
        if (type == 0) {
            js += "document.getElementById('imaContainer').innerHTML = '';";
        } else if (type == 1) {
            js += "document.getElementById('sdk__advertisement').innerHTML = '';";
        } else {
            js += "document.getElementById('imaContainer_new').style.display = 'none';";
        }
        js += "document.body.appendChild(newscript);";
        return js;
    }

    @Override
    public void onPause() {
        mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mAgentWeb.getWebLifeCycle().onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}