package com.xw.baselib.utils;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.xw.baselib.OnAdLoadListener;

/**
 * 插屏广告
 */
public class  InterstitialAdUtils {

    private InterstitialAd mInterstitialAd;
    private Activity activity;
    private OnAdLoadListener onAdLoadListener;

    public InterstitialAdUtils(Activity activity) {
        this.activity = activity;
    }

    public void setOnAdLoadListener(OnAdLoadListener onAdLoadListener) {
        this.onAdLoadListener = onAdLoadListener;
    }

    public void loadAD(String adIds) {
        if (onAdLoadListener != null) {
            onAdLoadListener.onStartLoading();
        }

        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(activity,
                adIds,
                adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // 插页式广告参考将为空，直到
                        // 广告已加载。
                        InterstitialAdUtils.this.mInterstitialAd = interstitialAd;
                        Log.i("info", "广告已加载");
                        showInterstitial();
                        interstitialAd.setFullScreenContentCallback(
                                new FullScreenContentCallback() {
                                    @Override
                                    public void onAdDismissedFullScreenContent() {
                                        //当全屏内容被关闭时调用。
                                        //确保将您的引用设置为空，这样您就不会
                                        //显示第二次。
                                        InterstitialAdUtils.this.mInterstitialAd = null;
                                        if (onAdLoadListener != null) {
                                            onAdLoadListener.onAdClose();
                                        }
                                        Log.d("TAG", "该广告被驳回。");
                                    }

                                    @Override
                                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                                        //当全屏内容无法显示时调用。
                                        //确保将您的引用设置为空，这样您就不会
                                        //显示第二次。
                                        InterstitialAdUtils.this.mInterstitialAd = null;
                                        Log.d("TAG", "广告未能展示。");
                                        if (onAdLoadListener != null) {
                                            onAdLoadListener.onError();
                                        }
                                    }

                                    @Override
                                    public void onAdShowedFullScreenContent() {
                                        // 当显示全屏内容时调用。
                                        Log.d("TAG", "广告被展示了。");
                                    }
                                });
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        //处理错误
                        Log.i("info", loadAdError.getMessage());
                        mInterstitialAd = null;
                        if (onAdLoadListener != null) {
                            onAdLoadListener.onError();
                        }
//                        String error =
//                                String.format(
//                                        "领域: %s, 代码: %d, 信息: %s",
//                                        loadAdError.getDomain(), loadAdError.getCode(), loadAdError.getMessage());

//                        Log.d("info","在AdFailedToLoad()上出现错误： " + error);
                    }
                });
    }

    private void showInterstitial() {
        if (onAdLoadListener != null) {
            onAdLoadListener.onLoadFinish();
        }
        //如果广告准备好了，就显示它。否则就重新启动游戏
        if (mInterstitialAd != null) {
            mInterstitialAd.show(activity);
        } else {
            Log.d("info","广告没有加载");
        }
    }
}
