package com.xw.baselib.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

public class SplashDialogUtils {

    private Activity activity;

    public SplashDialogUtils(Activity activity) {
        this.activity = activity;
    }

    public void showVPNDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Tips")
                .setMessage("VPN detected! Please turn off VPN to resume")
                .setPositiveButton("yes", (dialog, which) -> System.exit(0));
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void showUploadDialog(String upgradeUrl) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Tips")
                .setMessage("A new and more useful version has been released, please upgradeï¼");
        builder.setPositiveButton("yes", (dialog, which) -> {
            if (TextUtils.isEmpty(upgradeUrl)) {
                Toast.makeText(activity, "The upgradeUrl cannot be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            Uri uri = Uri.parse(upgradeUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            activity.startActivity(intent);
        });

        builder.setNegativeButton("no", (dialog, which) -> {
            System.exit(0);
        });

        builder.create().show();
    }
}
