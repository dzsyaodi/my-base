package com.xw.baselib.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;

import com.xw.baselib.openad.AppOpenAdManager;

/**
 * 开屏广告
 */
public class OpenAdUtils {

    private AppOpenAdManager appOpenAdManager;
    private Activity activity;

    public OpenAdUtils(Activity activity,String openId) {
        this.activity = activity;
        appOpenAdManager = new AppOpenAdManager(activity,openId);
    }

    public void showOpenAd(long seconds,String className) {
        CountDownTimer countDownTimer =
                new CountDownTimer(seconds * 1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {

                        appOpenAdManager.showAdIfAvailable(() -> {
                            Intent intent = new Intent();
                            try {
                                Class<?> cls = Class.forName(className);
                                intent.setClass(activity,cls);
                                activity.startActivity(intent);
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                };
        countDownTimer.start();
    }
}
