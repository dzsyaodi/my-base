package com.xw.baselib.openad;

public interface OnShowAdCompleteListener {
    void onShowAdComplete();
}
